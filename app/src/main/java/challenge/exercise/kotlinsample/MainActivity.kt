package challenge.exercise.kotlinsample

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import challenge.exercise.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.question_list_item.view.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private val QUESTION_LIST_DATA_KEY : String = "QUESTION_LIST_DATA_KEY"

    private lateinit var questionListAdapter : QuestionListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //init RecyclerView and Adapter
        questionList.layoutManager = LinearLayoutManager(this)
        questionListAdapter = QuestionListAdapter(getListOfQuestions(savedInstanceState), this)
        questionList.adapter = questionListAdapter

    }


    private fun getListOfQuestions(savedInstanceState : Bundle?) : ArrayList<TriviaQuestion> {
        return if (savedInstanceState != null && savedInstanceState.containsKey(QUESTION_LIST_DATA_KEY)) { // load saved data if it's available
            val array = savedInstanceState.getParcelableArray(QUESTION_LIST_DATA_KEY)
            array?.toCollection(ArrayList()) as ArrayList<TriviaQuestion>

        } else {
            MockRepo().triviaQuestions // get data from MockRepo

        }
    }


    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        val array = arrayOfNulls<TriviaQuestion>(questionListAdapter.items.size)
        questionListAdapter.items.toArray(array)
        savedInstanceState.putParcelableArray(QUESTION_LIST_DATA_KEY, array)

        super.onSaveInstanceState(savedInstanceState)
    }


    class QuestionListAdapter(val items : ArrayList<TriviaQuestion>, private val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent.getContext())
            val view = inflater.inflate(R.layout.question_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.setTriviaQuestion(items[position])

            holder.questionLayout.setOnClickListener {// set Click listener
                items[position].isChecked = true
                startAnswerActivity(items[position])
            }
        }

        override fun getItemCount(): Int {
            return items.size
        }

        private fun startAnswerActivity(triviaQuestion : TriviaQuestion) {
            val intent = Intent(context, AnswerActivity::class.java)
            intent.putExtra(AnswerActivity.DATA_KEY,triviaQuestion)
            context.startActivity(intent)

        }

    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        private var triviaQuestion : TriviaQuestion? = null
        private val question = view.tvQuestion
        var questionLayout : LinearLayout = view.questionLayout
        var tvAnswer = view.tvAnswer

        init {
            questionLayout.setOnClickListener {
                triviaQuestion?.isChecked = true

                if (triviaQuestion?.isChecked!!) {
                    tvAnswer.text = triviaQuestion?.answer
                    tvAnswer.visibility = View.VISIBLE
                }
            }
        }

        fun setTriviaQuestion(triviaQuestion : TriviaQuestion) {
            this.triviaQuestion = triviaQuestion
            question.text = triviaQuestion.question

            if (triviaQuestion.isChecked) {
                tvAnswer.visibility = View.VISIBLE
                tvAnswer.text = triviaQuestion.answer
            }
        }

    }

}
