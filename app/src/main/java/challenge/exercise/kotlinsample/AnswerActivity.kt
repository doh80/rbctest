package challenge.exercise.kotlinsample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import challenge.exercise.R
import kotlinx.android.synthetic.main.activity_answer.*

class AnswerActivity : AppCompatActivity() {

    companion object {
        val DATA_KEY : String = "DATA_KEY"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer)

        intent?.let {
            if (intent.hasExtra(DATA_KEY)) {
                val triviaQuestion = intent.getParcelableExtra<TriviaQuestion>(DATA_KEY)
                updateUi(triviaQuestion)
            }
        }

    }


    private fun updateUi(triviaQuestion : TriviaQuestion) {
        question.text = triviaQuestion.question
        answer.text = triviaQuestion.answer

    }

}
